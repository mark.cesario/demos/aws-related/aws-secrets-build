#  AWS Secrets Manager Wrapper

Based on (https://hub.docker.com/r/clevy/awssecrets) for [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/?nc1=h_ls). Will simply output the requested secrets to console.  
Useful for loading secrets in a env file or exporting in the environment, for example in CI stages.

The service is called awssecretsmgr.

This will try to retrieve permission from its environment, so either a resource role or user credentials with `secretsmanager:GetSecretValue` permissions are required.

With [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/?nc1=h_ls), automatic rotation and permission can be setup for secrets.

## Gitlab CI and Runners

Set the AWS Secrets Manager access in the CI variables masked.

Can be used by shared runners and on prem runners.

Here is what a `.gitlab-ci.yml` could look like:

```
stages:
  - secrets
  - deploy

read_secrets:
  stage: test
  image: GitLab_SecretsMgrWrapper_Image
  script:
    - awssecretsmgr --region eu-west-1 --secret my-secrets > .ci-secrets
    - cat .ci-secrets
    - export $(cat .ci-secrets | xargs)
    - echo $port
  artifacts:
    # you want to set this to a value high enough to be used in all your stages,
    # but low enough that it gets invalidated quickly and needs to be regenerated later
    expire_in: 30 min 
    paths:
      # this will be available by default in all the next stages
      - .ci-secrets

release:
  stage: deploy
  image: node
  before_script:
    # you can import the content of the secrets file in your environment like so
    - export $(cat .ci-secrets | xargs)
    - echo "${IMAGE_REGISTRY}:_authToken=${NPM_TOKEN}" >> ~/.npmrc
  script:
    - npm install -q
    - npm publish

```


### Usage: Arguments

```
mandatory:
--region (default: `eu-west-1`): region in which the secret is stored
--secret (default: none): name or ARN of secret to retrieve

optional:
--AWS_ACCESS_KEY_ID (default: from environment)
--AWS_SECRET_ACCESS_KEY (default: from environment)
```
